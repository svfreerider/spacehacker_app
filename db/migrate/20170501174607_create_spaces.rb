class CreateSpaces < ActiveRecord::Migration[5.0]
  def change
    create_table :spaces do |t|
      t.string :name
      t.string :genre
      t.string :minimum_fee
      t.string :access
      t.string :hour
      t.string :address
      t.string :phone
      t.string :website
      t.integer :seats
      t.text :amenities
      t.text :plan
      t.text :guide
      t.text :description

      t.timestamps
    end
  end
end
