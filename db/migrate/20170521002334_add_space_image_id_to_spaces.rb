class AddSpaceImageIdToSpaces < ActiveRecord::Migration[5.0]
  def self.up
    add_column :spaces, :space_image_id, :integer
    add_index :spaces, :space_image_id
  end

  def self.down
    remove_index :spaces, :column => :space_image_id
    remove_column :spaces, :space_image_id
  end
end
