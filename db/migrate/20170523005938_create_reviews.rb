class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.text :title
      t.integer :rate
      t.text :content
      t.references :user, foreign_key: true
      t.references :space, foreign_key: true

      t.timestamps
    end
    add_index :reviews, [:user_id, :space_id, :created_at]
  end
end
