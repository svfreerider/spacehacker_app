class AddMapToSpaces < ActiveRecord::Migration[5.0]
  def change
    add_column :spaces, :maptitle, :string
    add_column :spaces, :mapdescription, :string
    add_column :spaces, :mapaddress, :string
    add_column :spaces, :latitude, :float
    add_column :spaces, :longitude, :float
  end
end
