class ChangeNameOptionsToUsers < ActiveRecord::Migration[5.0]
  def up
    change_column :users, :name, :string, null: false, default: ""
    change_column :users, :background, :string
  end

  def down
    change_column :users, :name, :string, null: false, default: ""
    change_column :users, :background, :string
  end
end
