class AddTitleToSpaceImages < ActiveRecord::Migration[5.0]
  def change
    add_column :space_images, :images_title, :string
  end
end
