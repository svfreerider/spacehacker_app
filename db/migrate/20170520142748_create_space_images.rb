class CreateSpaceImages < ActiveRecord::Migration[5.0]
  def change
    create_table :space_images do |t|
      t.string :image_1
      t.string :image_2
      t.string :image_3
      t.string :image_4
      t.string :image_5
      t.string :image_6
      t.string :image_7
      t.string :image_8
      t.string :image_9

      t.timestamps
    end
  end
end
