class AddAreaIdToSpaces < ActiveRecord::Migration[5.0]
  def self.up
    add_column :spaces, :area_id, :integer
    add_index :spaces, :area_id
  end

  def self.down
    remove_index :spaces, :column => :area_id
    remove_column :spaces, :area_id
  end
end
