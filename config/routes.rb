Rails.application.routes.draw do

  get 'users/show'

  devise_for :users, controllers: { registrations: 'users/registrations',
    omniauth_callbacks: 'users/omniauth_callbacks',
    sessions: 'users/sessions', confirmations: 'users/confirmations',
    passwords: 'users/passwords'
    }
  devise_scope :user do
    get '/users/sign_out', to: 'users/sessions#destroy'
  end
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'map/index'
  get 'areas/show'

  root 'static_pages#home'
  get'/about', to: 'static_pages#about'
  get'/contact', to: 'static_pages#contact'
  get'/advertise', to: 'static_pages#advertise'
  get'/terms', to: 'static_pages#terms'
  get'/policy', to: 'static_pages#policy'
  get '/', to: 'areas#show'

  resources :areas, param: :slag do
    resources :spaces, only: [:show] do
      member do
        get 'review'
        get 'photo'
        get 'map'
      end
    end
  end

  resources :spaces, only: [:show] do
    collection do
      get 'search'
    end
  end

  resources :reviews
  resources :users, only: [:show]
end
