require 'test_helper'

class UserMailerTest < ActionMailer::TestCase

  test "account_activation" do
    user = users(:michael)
    user.activation_token = User.new_token
    mail = UserMailer.account_activation(user)
    assert_equal "【スペースハッカー】本登録のお願い", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["sy@gociao.com"], mail.from
  end

  test "password_reset" do
    user = users(:michael)
    user.reset_token = User.new_token
    mail = UserMailer.password_reset(user)
    assert_equal "【スペースハッカー】パスワードの再設定", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["sy@gociao.com"], mail.from
  end
end
