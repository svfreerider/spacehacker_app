require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "スペースハッカー"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "スペースハッカー - スペースハッカーについて"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "スペースハッカー - お問い合わせ"
  end

  test "should get advertise" do
    get advertise_path
    assert_response :success
    assert_select "title", "スペースハッカー - 広告掲載"
  end

  test "should get terms" do
    get terms_path
    assert_response :success
    assert_select "title", "スペースハッカー - 利用規約"
  end

  test "should get policy" do
    get policy_path
    assert_response :success
    assert_select "title", "スペースハッカー - プライバシーポリシー"
  end
end
