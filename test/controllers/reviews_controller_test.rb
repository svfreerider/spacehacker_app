require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @review = reviews(:most_recent)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Review.count' do
      post reviews_path, params: { micropost: { title: "Thanks so much", content: "こんな広くて素晴らしい場所が無料なんて信じがたい！大学の学食みたいな開放感がサイコー！なんと小学生も大人同伴なら入れるから社会科見学的に親子で使うのもいいです！" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Review.count' do
      delete review_path(@review)
    end
    assert_redirected_to login_url
  end
end
