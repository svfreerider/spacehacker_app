require 'test_helper'

class SpaceTest < ActiveSupport::TestCase

  def setup
    @space = Space.new(name: "HACKERS SPACE", genre: "コワーキングスペース", minimum_fee: "1時間500円〜",
      access: "千代田線・副都心線「明治神宮前」駅", hour: "10:00〜20:00", address: "東京都渋谷区神宮前1丁目2-3 スペースハックビル4F",
      phone: "090-7614-7412", website: "	http://gociao.com", seats: 40,
      amenities: "電源あり / WiFiあり / フリードリンクあり / 会議室あり / モニター貸出あり",
      plan: "ドロップイン：2時間500円、1日1000円",
      guide: "出口を出たら、表参道方面に向かって歩きます。セブンイレブンの角を左に曲がって、すぐにビルが見えてきます。",
      description: "原宿駅から徒歩1分で来れるコワーキングスペース。原宿という場所柄、たくさんの会社員やフリーランスの方々にご利用いただいております。もちろん、会社員やフリーランスだけでなく、学生や主婦の方など、たくさんの方々にご利用いただけるコワーキングスペースとなっております。 また、貸会議室も併設しておりますので、一人で仕事をするときだけではなく、チームでミーティングするときなどにも、ご利用いただけます。")
  end

  test "should be valid" do
    assert @space.valid?
  end
end
