ActiveAdmin.register SpaceImage do
  permit_params :images_title, :space_id, :space, :image_1, :image_2, :image_3,:image_4, :image_5, :image_6, :image_7, :image_8, :image_9
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
end
