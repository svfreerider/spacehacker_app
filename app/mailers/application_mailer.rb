class ApplicationMailer < ActionMailer::Base
  default from: "sy@gociao.com"
  layout 'mailer'
end
