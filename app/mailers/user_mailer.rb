class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "【スペースハッカー】本登録のお願い"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "【スペースハッカー】パスワードの再設定"
  end
end
