class Space < ApplicationRecord
  has_many :reviews
  belongs_to :area
  belongs_to :space_image

  accepts_nested_attributes_for :space_image
  mount_uploader :image, ImageUploader
  geocoded_by :mapaddress
  after_validation :geocode
end
