class Review < ApplicationRecord
  belongs_to :user
  belongs_to :space
  default_scope -> { order(created_at: :desc)}
  validates :user_id, presence: true
  # validates :space_id, presence: true
  validates :content, presence: true, length: { maximum: 2000, minimum: 100 }
end
