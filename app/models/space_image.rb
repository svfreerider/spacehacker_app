class SpaceImage < ApplicationRecord
  has_many :spaces
  mount_uploader :image_1, SpaceImageUploader
  mount_uploader :image_2, SpaceImageUploader
  mount_uploader :image_3, SpaceImageUploader
  mount_uploader :image_4, SpaceImageUploader
  mount_uploader :image_5, SpaceImageUploader
  mount_uploader :image_6, SpaceImageUploader
  mount_uploader :image_7, SpaceImageUploader
  mount_uploader :image_8, SpaceImageUploader
  mount_uploader :image_9, SpaceImageUploader
end
