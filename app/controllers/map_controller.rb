class MapController < ApplicationController
  def index
    @hash = Gmaps4rails.build_markers(@spaces) do |space, marker|
      marker.lat space.latitude
      marker.lng space.longitude
      marker.infowindow space.mapdescription
      marker.json({title: space.maptitle})
    end
  end
end
