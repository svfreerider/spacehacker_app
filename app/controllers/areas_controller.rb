class AreasController < ApplicationController
  def show
    @area = Area.find_by(slag: params[:slag])
    @spaces = Space.paginate(:page => params[:page], :per_page => 10)
  end
end
