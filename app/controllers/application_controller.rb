class ApplicationController < ActionController::Base
  before_action :set_search
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def set_search
    @search = Space.ransack(params[:q])
    @search_spaces = @search.result.paginate(page: params[:page])
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :background, :image])
  end

  private

  def signed_in_user
    unless signed_in?
      store_location
      flash[:danger] = "ログインしてください"
      redirect_to login_url
    end
  end
end
