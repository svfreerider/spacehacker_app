class SpacesController < ApplicationController
  before_action :set_space, only:[:show, :review, :photo, :map]
  add_breadcrumb 'スペースハッカー', :root_path
  add_breadcrumb 'エリア検索', :areas_show_path
  add_breadcrumb 'スペース', :area_space_path

  def new
      @space = Space.new(space_params)
      9.times { @space.space_images.build }
  end

  def review
    @reviews = Review.paginate(:page => params[:page], :per_page => 10)
  end

  def photo
  end

  def map
    @hash = Gmaps4rails.build_markers(@spaces) do |space, marker|
      marker.lat space.latitude
      marker.lng space.longitude
      marker.infowindow space.mapdescription
      marker.json({title: space.maptitle})
    end
  end

  def search
    @spaces = Space.paginate(page: params[:page], :per_page => 10)
    @areas = Area.all
  end
  private

  def space_params
    params.require(:space).permit(:name, :image, :genre, :minimum_fee, :access, :hour,
     :address, :phone, :website, :amenities, :plan, :guide,
      :description, :seats, :area_id, :space_image_id)
  end

  def set_space
    @space = Space.find(params[:id])
    @review = Review.new
  end

  def review_feed
    if logged_in?
      @review = current_user.reviews.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
end
