class ReviewsController < ApplicationController
  before_action :signed_in_user
  before_action :correct_user, only: :destroy

  def create
   @review = current_user.reviews.build(review_params)
   if @review.save
     flash[:success] = "コメントが投稿されました！"
     redirect_to root_path
   else
     #flash.now[:alert] = 'コメントの投稿に失敗しました。'
     render 'new'
   end
 end

  def show
    @review = Review.find(params[:id])
  end

  def new
    @review = current_user.reviews.build if signed_in?
  end

  def destroy
    @review.destroy
    flash[:success] = "口コミが削除されました"
    redirect_to request.referrer || root_url
  end

  def edit
  end

  def update
  end

  private

  def review_params
    params.require(:review).permit(:id,:content, :rate, :title, :space_id, :user_id)
  end

  def correct_user
    @review = current_user.reviews.find_by(id: params[:id])
    redirect_to root_url if @review.nil?
  end
end
