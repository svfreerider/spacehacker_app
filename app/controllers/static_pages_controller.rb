class StaticPagesController < ApplicationController
  def home
    @users = User.all
    @spaces = Space.all
    @reviews = Review.all
    @areas = Area.all
  end

  def about
  end

  def contact
  end

  def advertise
  end

  def terms
  end

  def policy
  end

end
