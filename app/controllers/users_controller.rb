class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @reviews = @user.reviews.paginate(page: params[:page], :per_page => 10)
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confiramation, :image, :background)
  end
end
